<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

use App\Models\Transaction;

class TransactionController extends Controller
{
    public function helloWorld(Request $request){
        return response()->json(['status' => 1, 'msg' => 'Hello There']);
    }



    public function firstApi(Request $request){
        //CHECK IF THE REQUIRED HEADER IS SET 
        if($request->hasHeader('X-Mock-Status'))
        {
            //GENERATE RANDOM STATUS
            $possibleStatusValues = ['accepted', 'failed'];
            $randomKey = array_rand($possibleStatusValues, 1);

            //RETURN RESPONSE
            return response()->json(['status' => $possibleStatusValues[$randomKey]], 200);
        }
        else
        {
            //RETURN 'INVALID REQUEST'
            return response()->json(['status' => 'invalid request, header missing'], 400);
        }
    }



    public function secondAPI(Request $request){
        if($request->isMethod('post'))
        {
            //VALIDATE
            $this->validate($request, [
                'user_id' => 'required|integer',
                'amount' => 'required|numeric',
            ]);


            //CALL FIRST API TO FETCH PAYMENT STATUS
            $paymentStatus = $this->fetchPaymentStatus();

        
            //STORE TRANSACTION DATA
            $transaction = new Transaction();
            $transaction->user_id = $request->user_id;
            $transaction->amount = $request->amount;
            $transaction->transaction_id = Str::uuid()->toString();
            $transaction->status = $paymentStatus;

            if($transaction->save())
            {
                return response()
                            ->json(['status' => 'transaction stored'], 201)
                            ->withHeaders(['Cache-Control' => 'no-store']);
            }
            else
            {
                return response()
                            ->json(['status' => 'something went wrong when saving transaction information'], 200)
                            ->withHeaders(['Cache-Control' => 'no-store']);
            }
        }
        else
        {
            //RETURN 'USE POST'
            return response()->json(['status' => 'use post'], 405);
        }
    }



    public function thirdApi(Request $request){
        if($request->isMethod('post'))
        {
            //VALIDATE
            $this->validate($request, [
                'transaction_id' => 'required|exists:transactions,transaction_id',
                'status' => 'required|in:accepted,failed',
            ]);

            //UPDATE TRANSACTION STATUS
            $transaction = Transaction::where('transaction_id', $request->transaction_id)->first();
            $transaction->status = $request->status;

            if($transaction->save())
            {
                return response()->json(['status' => 'transaction updated'], 201);
            }
            else
            {
                return response()->json(['status' => 'something went wrong when saving transaction information'], 200);
            }
        }
        else
        {
            //RETURN 'USE POST'
            return response()->json(['status' => 'use post'], 405);
        }
    }


    private function fetchPaymentStatus(){
        //CALL FIRST API
        $apiRequest = Request::create('/api/first-api', 'GET');
        $apiRequest->headers->set('X-Mock-Status','yes'); //this snippet is not working as expected !!
        $response = Route::dispatch($apiRequest);

        //PROCESS RESPONSE
        $responseDecoded = json_decode($response->getContent(),true);
        $mockStatus = $responseDecoded['status'];

        if(!in_array($mockStatus, ['accepted', 'failed']))
        {
            return 'failed';
        }
        {
            return $mockStatus;
        }
    }
}
