<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransactionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('hello', [TransactionController::class, 'helloWorld'])->name('hello');
Route::get('first-api',[TransactionController::class, 'firstApi'])->name('api.firstApi');
Route::post('second-api',[TransactionController::class, 'secondApi'])->name('api.secondApi');
Route::post('third-api',[TransactionController::class, 'thirdApi'])->name('api.thirdApi');
